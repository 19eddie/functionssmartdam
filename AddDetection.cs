using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos;

namespace SmartDam
{
    public static class AddDetection
    {
        [FunctionName("AddDetection")]
        public static async Task<IActionResult> Run(
            //trigger http
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            //binding cosmos db
            [CosmosDB(
                databaseName: "smartDam",
                collectionName: "rilevazioni",
                ConnectionStringSetting = "CosmosDbConnectionString")]IAsyncCollector<dynamic> documentsOut,
            //console log
            ILogger log)
        {
            //log che appare nella console
            log.LogInformation("C# addDetection HTTP trigger function processed a request.");
            //legge il body della richiesta http
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();

            Detection detection = new Detection();
            string responseMessage="";
            try
            {
                //si aspetta che nel body ci siano dei dati in formato json
                dynamic data = JsonConvert.DeserializeObject(requestBody);

                detection.State = data.state;
                detection.WaterLevel = data.water_level;
                //data e ora con fuso orario italiano
                detection.Time = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time")).ToString();

                //log sulla console e messaggio http di riposta
                responseMessage = $"The system detected a water level of {detection.WaterLevel} at {detection.Time}. The system is in {detection.State} mode";
                log.LogInformation(responseMessage);
            }
            catch (Exception)
            {
                //log sulla console e messaggio http di riposta se non va a buon fine
                responseMessage = "An error has occurred while reading the Json body of the HTTP post request.";
                log.LogInformation(responseMessage);
            }

            //scrive la nuova detection nel database
            await documentsOut.AddAsync(detection);

            //risposta http 200 ok con il messaggio indicato 
            return new OkObjectResult(responseMessage);
        }
    }
    public class Detection
    {
        public string Time { get; set; }
        public string WaterLevel { get; set; }
        public string State { get; set; }
    }
}
