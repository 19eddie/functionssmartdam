Questa applicazione è stata realizzata per fare pratica con lo sviluppo delle Functions in locale, utilizzando Visual Studio.

Lo scopo dell’applicazione è il seguente: 
ricevere delle misurazioni del livello dell’acqua di una ipotetica diga, salvarle in un database ed inviare una e-mail nel caso in cui la diga sia in stato di allarme.

Per realizzarla è stato necessario utilizzare i seguenti servizi: Azure Functions, Azure Cosmos DB, SendGrid (per inviare e-mail).