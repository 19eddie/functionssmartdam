using System;
using System.Collections.Generic;
using Microsoft.Azure.Documents;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using SendGrid.Helpers.Mail;

namespace SmartDam
{
    public static class Notify
    {

        [FunctionName("Notify")]
        public static void Run(
            //binding trigger cosmosDB
            [CosmosDBTrigger(
            databaseName: "smartDam",
            collectionName: "rilevazioni",
            ConnectionStringSetting = "CosmosDbConnectionString",
            LeaseCollectionName = "leases",
            CreateLeaseCollectionIfNotExists = true)]IReadOnlyList<Document> input,
            //binding output email
            [SendGrid(ApiKey = "CustomSendGridKeyAppSettingName")] out SendGridMessage message,
            //console log
            ILogger log)
        {
            //se il sistema � in allarme invia l'email
            if (input[0].GetPropertyValue<String>("State").Equals("ALARM"))
            {
                OutgoingEmail emailObject = new OutgoingEmail
                {
                    To = System.Environment.GetEnvironmentVariable("emailTo", EnvironmentVariableTarget.Process),
                    From = System.Environment.GetEnvironmentVariable("emailFrom", EnvironmentVariableTarget.Process),
                    Subject = "The Smart Dam is in ALARM!",
                    Body = $"The system detected a water level of {input[0].GetPropertyValue<String>("WaterLevel")} cm at {input[0].GetPropertyValue<String>("Time")}. The system is in ALARM!"
                };
                message = new SendGridMessage();
                message.AddTo(emailObject.To);
                message.AddContent("text/html", emailObject.Body);
                message.SetFrom(new EmailAddress(emailObject.From));
                message.SetSubject(emailObject.Subject);

                log.LogInformation("Object of the email: " + emailObject.Subject);
                log.LogInformation("Receiver of the email: " + emailObject.To);
            }
            else //altrimenti no
            {
                message = null;
                log.LogInformation("The system is not in Alarm. Don't need to send an email.");
            }

        }
    }
    public class OutgoingEmail
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
